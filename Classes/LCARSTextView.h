//
//  LCARSTextView.h
//  LCARS Wikipedia
//
//  Created by Danny Draper on 13/01/2011.
//  Copyright 2011 CedeSoft Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SmallAlpha.h"
#import "SingleWord.h"
#import <Foundation/NSXMLParser.h>
#import "ParserEngine.h"
#import "HTMLParser.h"
#import "WikiPart.h"
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVAudioPlayer.h>

#define PLAINTEXT		0
#define LINKTAG			100
#define TEMPLATETAG		101
#define HEADERTAG		102
#define SUBHEADERTAG	103
#define ITALICTAG		104
#define BOLDTAG			105
#define EXTRABOLD		106

#define FILETAG			200
#define WIKILINKTAG			201


@protocol ParseView

- (void)requestRedirect:(NSString *)targettitle;
- (void)displayReady;
- (void)wikiarticleNotfound;
- (void)redirectRequestprocessing;
- (void) resetSummaryshown;

@end

@interface LCARSTextView : UIView <NSXMLParserDelegate, ParsingEngine, HTMLParsingEngine, AVAudioPlayerDelegate> {

	id _delegateobject;
	SmallAlpha *_lcarsstring;
	SmallAlpha *_lcarsstringorange;
	SmallAlpha *_lcarsstringyellow;	
	
	SmallAlpha *_teststring;
	
	
	NSArray *_wordlist;
	NSMutableArray *_wordobjects;
	NSMutableArray *_wikiparts;
	NSMutableArray *_wordobjectsbackup;
	
	NSMutableArray *_pagearray;
	NSMutableString *_pagelabel;
	bool _redirecttofirstlink;
	bool _redirectflag;
	
	CGRect _currentrect;
	CGRect _lastwordrect;
	bool _wordsavailable;
	NSXMLParser *_xmlparser;
	bool _wantedxmlchars;
	NSMutableString *_wantedxmlselection;
	NSMutableString *_testwantedxml;
	
	NSMutableString *_articlesummarystring;
	NSMutableString *_articlesummarybackup;
	NSMutableArray *_articlesummarylist;
	
	
	//int _ttwordlimit;
	int _ttcurrentword;
	
	int _currentpage;
	int _currentpagebackup;
	int _numpages;
	
	bool _wikixmlfound;
	
	ParserEngine *_parserengine;
	HTMLParser *_htmlparser;
	
	NSTimer *_teletypeTimer;
	
	NSMutableString *_htmlparsedoutput;
	
	AVAudioPlayer* _myaudioplayerone;
	bool _audioplayingone;
	
	AVAudioPlayer* _myaudioplayertwo;
	bool _audioplayingtwo;
}

- (void) configureAudioServices;
- (void)playSinglesoundChOne:(NSString *)resourcename;
- (void)playSinglesoundChTwo:(NSString *)resourcename;

- (void) setDelegate:(id)delegate;
- (void) preparePage:(int)wantedpage;
- (void) resetParser;
- (void)displayArticlesummary;
- (void)restoreArticlecontent;
- (bool) containsString:(NSString *)contents :(NSString *)searchingfor;
- (void)buildArticlesummarymarkup;
- (bool)doesArticlereferenceexist:(NSString *)summaryname;
- (void)addArticlereference:(NSString *)summaryname:(int)pagenumber;
- (void) sendRedirectrequest:(NSString *)target;
- (void) triggerTeletype:(id)sender;
- (void)setViewframe:(CGRect)viewframe;
- (void) startTextrendering:(NSString *)stringtorender;
- (void) startDirecttextrender:(NSString *)stringtorender;
- (int)calculateNumpages:(float)startcursorx :(float)startcursory :(float)viewwidth :(float)viewheight;
- (void) createWordobjects:(NSString *)stringtorender:(LCARSColor)wordcolor:(WordType)wordtype:(NSString *)target:(bool)hasnewline;
- (void) parseXML:(NSString *)xmldata;
- (void) beginWikiparsing:(NSString *)wikimarkupstring;
- (bool) isPointWithinRect:(CGPoint)point:(CGRect)rect;
- (void)checkWikiLinkTouched:(CGPoint)touchpoint;
- (void) DisplayPage:(int)pagenumber;
- (void) NextPage;
- (void) PrevPage;
- (NSString *) getPagelabel;
- (void)clearDisplay;

@end
