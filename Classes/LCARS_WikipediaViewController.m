//
//  LCARS_WikipediaViewController.m
//  LCARS Wikipedia
//
//  Created by Danny Draper on 12/01/2011.
//  Copyright 2011 CedeSoft Ltd. All rights reserved.
//

#import "LCARS_WikipediaViewController.h"

@implementation LCARS_WikipediaViewController

@synthesize lcarstextview;

// LCARS Keyboard ImageView
@synthesize keyboardview;
@synthesize imgview_enterkey;
@synthesize pagelabel;

@synthesize imgview_working;

/*
// The designated initializer. Override to perform setup that is required before the view is loaded.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
*/

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/



// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	
	[lcarstextview setDelegate:self];
	
	NSLog (@"Initiating the connection...");

	_dataisavailable = false;
	_summaryshown = false;

	//[self InitialiseConnection:@"Wikipedia"];
	
	[lcarstextview startDirecttextrender:@"Welcome to LCARS Wikipedia. To begin tap the search button above and type the name of the entry you would like to search using the LCARS Keyboard. Tap the Engage button to begin a search for your specified entry. The Article Summary button on the left will list the main article headings and sub headings. Tap the heading to jump directly to that page in the article. Tap the article summary button again to toggle back to the article. Text that is coloured orange in the article represent links. Tap any orange text to jump directly to that article."];
	
	//[self LoadFromFile];
	
	//[lcarstextview setViewframe:CGRectMake(20.0f, 20.0f, 347.0f, 347.0f)];
	
	
	NSLog(@"Lcars width: %i", lcarstextview.frame.size.width);
	
		 
	keyboardview.hidden = true;
	//imgview_working.hidden = true;
	
	// 6.0f, 762.0f, 757.0f, 235.0f
	
	keyboardview.frame = CGRectMake(6.0f, 762.0f, 757.0f, 235.0f);
	
	[searchlabel setCaption:@""];
	
    [super viewDidLoad];
}

- (void) showWorkingimage
{
	[imgview_working setImage:[UIImage imageNamed:@"Working.png"]];
	[imgview_working sizeToFit];
}

- (void) showNotfoundimage
{
	[imgview_working setImage:[UIImage imageNamed:@"NotFound.png"]];
	[imgview_working sizeToFit];	
}

- (void) showErrorimage
{
	[imgview_working setImage:[UIImage imageNamed:@"NoInternet.png"]];
	[imgview_working sizeToFit];
}

- (void) requestRedirect:(NSString *)targettitle
{
	imgview_working.hidden = false;
	[self InitialiseConnection:targettitle];
	
	[self showWorkingimage];
	imgview_working.hidden = false;
}

- (void)redirectRequestprocessing
{
	[self showWorkingimage];
	imgview_working.hidden = false;
}

- (void) displayReady
{
	imgview_working.hidden = true;
	[pagelabel setCaption:[lcarstextview getPagelabel]];
}

- (void) resetSummaryshown
{
	_summaryshown = false;
}

- (void)wikiarticleNotfound
{
	imgview_working.hidden = false;
	[self showNotfoundimage];
	[lcarstextview playSinglesoundChOne:@"Error"];
}

- (void) InitialiseConnection:(NSString *)articletitle
{
	// Create the request.
	[lcarstextview clearDisplay];
	[self showWorkingimage];
	
	imgview_working.hidden = false;
	
	NSMutableString *wikipediaapiquery;
	NSMutableString *resolvedtitle;
	
	wikipediaapiquery = [[NSMutableString alloc] init];
	resolvedtitle = [[NSMutableString alloc] init];
	
	[resolvedtitle setString:[articletitle stringByReplacingOccurrencesOfString:@" " withString:@"%20"]];
	
	[wikipediaapiquery appendString:@"http://en.wikipedia.org//w/api.php?action=query&titles="];
	[wikipediaapiquery appendString:resolvedtitle];
	[wikipediaapiquery appendString:@"&prop=revisions&rvprop=content&format=xml"];
	
	
	//http://en.wikipedia.org/w/api.php?action=query&titles=Albert%20Einstein&prop=revisions&rvprop=content
	//http://www.dannydraper.co.uk/sampledata.txt
	
	//NSURLRequest *theRequest=[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://www.dannydraper.co.uk/sampledata.txt"] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
	//NSURLRequest *theRequest=[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://en.wikipedia.org/w/api.php?action=query&titles=Albert%20Einstein&prop=revisions&rvprop=content&format=xml"] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
	NSURLRequest *theRequest=[NSURLRequest requestWithURL:[NSURL URLWithString:wikipediaapiquery] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
	//NSURLRequest *theRequest=[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://en.wikipedia.org/w/api.php?action=query&titles=theoretical%20physics&prop=revisions&rvprop=content&format=xml"] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
	
	
	//NSURLRequest *theRequest=[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://www.dannydraper.co.uk/testtext.txt"] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
	//NSURLRequest *theRequest=[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://www.apple.com/"] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
	//NSURLRequest *theRequest=[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://www.nasa.gov/images/content/124415main_image_feature_380a_ys_full.jpg"] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
	

	//http://www.nasa.gov/images/content/124415main_image_feature_380a_ys_full.jpg
	
	// create the connection with the request
	// and start loading the data
	NSURLConnection *theConnection=[[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
	
	if (theConnection) {
		// Create the NSMutableData to hold the received data.
		// receivedData is an instance variable declared elsewhere.
		receivedData = [[[NSMutableData alloc] init] retain];
		//receivedData = [[NSMutableData data] retain];
		
				
		NSLog(@"Connection success.");
	} else {
		// Inform the user that the connection failed.
		NSLog(@"Connection failed.");
	}
	
	[resolvedtitle release];
	[wikipediaapiquery release];
}

- (void) LoadFromFile
{
	NSString *filePath = [[NSBundle mainBundle] pathForResource:@"wikipedia-eistein-apiresult" ofType:@"xml"];
	
	BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
	
	if (fileExists == TRUE) {
		NSLog (@"Data File Exists!");
		
		receivedData = [NSData dataWithContentsOfFile:filePath];
		
		if (receivedData) {
			_dataisavailable = true;
			_receivedstring = [[NSString alloc] initWithData:receivedData encoding:NSASCIIStringEncoding];
			
			[lcarstextview startTextrendering:_receivedstring];
		} else {
			NSLog(@"Loaded Data From File is not valid!");
		}
	} else {
		NSLog (@"Data File not found.");
	}
}


// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	
	if (interfaceOrientation == UIInterfaceOrientationLandscapeLeft) {
		return NO;
	}
	
	if (interfaceOrientation == UIInterfaceOrientationLandscapeRight) {
		return NO;
	}
	
	if (interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown) {
		return YES;
	}
	
	if (interfaceOrientation == UIInterfaceOrientationPortrait) {
		return YES;
	}
	
	return YES;
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    // Append the new data to receivedData.
    // receivedData is an instance variable declared elsewhere.
    NSLog(@"About to append some received data...");
	
	[lcarstextview playSinglesoundChOne:@"databeep"];
	
	[receivedData appendData:data];
	
	//NSString *aStr;
	
	//aStr = [[NSString alloc] initWithData:receivedData encoding:NSASCIIStringEncoding];
	
	//NSLog(@"Data: %@", aStr);
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    // release the connection, and the data object
    [connection release];
    // receivedData is declared as a method instance elsewhere
    [receivedData release];
	
    // inform the user
    NSLog(@"Connection failed! Error - %@", [error localizedDescription]);
	

	
	[lcarstextview startDirecttextrender:[error localizedDescription]];
	
	[self showErrorimage];
	imgview_working.hidden = false;
	
	[lcarstextview playSinglesoundChOne:@"Error"];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    // do something with the data
    // receivedData is declared as a method instance elsewhere
    NSLog(@"Succeeded! Received %d bytes of data",[receivedData length]);
	
	if ([receivedData length] > 0) {
		_dataisavailable = true;
		if (_receivedstring != nil) {
			NSLog (@"Releasing _receivedstring");
			[_receivedstring release];
		}
		
		_receivedstring = [[NSString alloc] initWithData:receivedData encoding:NSASCIIStringEncoding];
		
		//NSLog (@"Received string: %@", _receivedstring);

		[connection release];
		[receivedData release];
		
		[lcarstextview playSinglesoundChOne:@"BeginParsing"];
		
		//NSLog (@"Received String was (connectionDidFinishLoading - controller): %@", _receivedstring);
		
		[lcarstextview startTextrendering:_receivedstring];
		
		[pagelabel setCaption:[lcarstextview getPagelabel]];
	}
	
    // release the connection, and the data object

}

- (void) Engagesearch
{
	NSString *searchcaption = [searchlabel getCaption];
	keyboardview.hidden = true;
	_summaryshown = false;
	
	[lcarstextview playSinglesoundChOne:@"Engage"];
	
	[self InitialiseConnection:searchcaption];
}

- (IBAction) engageButton:(id)sender
{
	[self Engagesearch];
}


// LCARS Keyboard Stuff
- (IBAction) enterKeydown:(id)sender
{
	imgview_enterkey.alpha = 0.5;
	[self keyboardKeytriggered:@"ENTER"];
	
	//[lcarstextview playSinglesoundChOne:@"clockbutton"];
}

- (IBAction) enterKeyup:(id)sender
{
	imgview_enterkey.alpha = 1.0f;
	
}

- (IBAction) keyboardKeypressed:(id)sender
{
	UIButton *button = (UIButton *)sender;
	
	[self keyboardKeytriggered:[button titleForState:UIControlStateNormal]];
}

- (void) keyboardKeytriggered:(NSString *)keytitle
{
	NSLog (@"Key Triggered is: %@", keytitle);
	
	if ([keytitle isEqualToString:@"HIDEKEYBOARD"] == true) {
		keyboardview.hidden = true;
	} else {
	
		bool keytriggered = false;
		
		if ([keytitle isEqualToString:@"SPACE"] == true) {
			[searchlabel appendCaption:@" "];
			keytriggered = true;
		}
		
		if ([keytitle isEqualToString:@"ENTER"] == true) {
			[self Engagesearch];
			return;
		}
		
		if ([keytitle isEqualToString:@"LSHIFT"] == true) {
			return;
		}
		
		if ([keytitle isEqualToString:@"RSHIFT"] == true) {
			return;		
		}
		
		if ([keytitle isEqualToString:@"PGUP"] == true) {
			return;
		}
		
		if ([keytitle isEqualToString:@"PGDN"] == true) {
			return;		
		}
		
		if ([keytitle isEqualToString:@"UP"] == true) {
			return;		
		}
		
		if ([keytitle isEqualToString:@"DOWN"] == true) {
			return;		
		}
		
		if ([keytitle isEqualToString:@"LEFT"] == true) {
			return;		
		}
		
		if ([keytitle isEqualToString:@"RIGHT"] == true) {
			return;		
		}

		if ([keytitle isEqualToString:@"HOME"] == true) {
			return;		
		}
		
		if ([keytitle isEqualToString:@"END"] == true) {
			return;		
		}
		
		if ([keytitle isEqualToString:@"TAB"] == true) {
			return;		
		}
		
		if ([keytitle isEqualToString:@"{"] == true) {
			return;		
		}
		
		if ([keytitle isEqualToString:@"}"] == true) {
			return;		
		}
		
		if ([keytitle isEqualToString:@"\""] == true) {
			return;		
		}
		
		if ([keytitle isEqualToString:@"BKSP"] == true) {
			[searchlabel backspaceCaption];
			keytriggered = true;
		}
		
		// Should be done at the end of the checks.
		if (keytriggered == false) {
			[searchlabel appendCaption:[keytitle lowercaseString]];
		}
	}
	
	[lcarstextview playSinglesoundChOne:@"clockbuttonhigh"];
}

- (IBAction) activateKeyboard:(id)sender
{
	keyboardview.hidden = false;
	
	[searchlabel setCaption:@""];
	
	[lcarstextview playSinglesoundChOne:@"showkeyboard"];
}

- (IBAction) prevPagetriggered:(id)sender
{
	[lcarstextview PrevPage];
	[pagelabel setCaption:[lcarstextview getPagelabel]];
	
	[lcarstextview playSinglesoundChOne:@"clockbutton"];
}

- (IBAction) nextPagetriggered:(id)sender
{
	[lcarstextview NextPage];
	[pagelabel setCaption:[lcarstextview getPagelabel]];

	[lcarstextview playSinglesoundChOne:@"clockbutton"];
}

- (IBAction) articleSummarytriggered:(id)sender
{
	if (_summaryshown == false) {
		_summaryshown = true;
		[lcarstextview displayArticlesummary];
	} else {
		_summaryshown = false;
		[lcarstextview restoreArticlecontent];
	}

}

- (void)dealloc {
    [super dealloc];
}

@end
