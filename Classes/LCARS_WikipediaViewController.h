//
//  LCARS_WikipediaViewController.h
//  LCARS Wikipedia
//
//  Created by Danny Draper on 12/01/2011.
//  Copyright 2011 CedeSoft Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/NSURLConnection.h>
#import "LCARSTextView.h"
#import "LCARSLabelView.h"

@interface LCARS_WikipediaViewController : UIViewController<ParseView> {

	IBOutlet LCARSTextView *lcarstextview;
	IBOutlet LCARSLabelView *searchlabel;
	IBOutlet LCARSLabelView *pagelabel;
	
	IBOutlet UIView *keyboardview;
	
	NSMutableData *receivedData;
	bool _dataisavailable;
	bool _summaryshown;
	
	NSString *_receivedstring;
	
	
	
	// LCARS Keyboard References
	IBOutlet UIImageView *imgview_enterkey;
	
	IBOutlet UIImageView *imgview_working;
}

@property (retain, nonatomic) LCARSTextView *lcarstextview;

@property (retain, nonatomic) LCARSLabelView *pagelabel;

@property (retain, nonatomic) UIImageView *imgview_working;

// LCARS Keyboard properties
@property (retain, nonatomic) UIView *keyboardview;
@property (retain, nonatomic) UIImageView *imgview_enterkey;

- (IBAction) enterKeydown:(id)sender;
- (IBAction) enterKeyup:(id)sender;
- (IBAction) keyboardKeypressed:(id)sender;
- (IBAction) activateKeyboard:(id)sender;
- (IBAction) engageButton:(id)sender;
- (IBAction) prevPagetriggered:(id)sender;
- (IBAction) nextPagetriggered:(id)sender;
- (IBAction) articleSummarytriggered:(id)sender;

- (void) keyboardKeytriggered:(NSString *)keytitle;
- (void) Engagesearch;

- (void) InitialiseConnection:(NSString *)articletitle;
- (void) LoadFromFile;

- (void) showErrorimage;
- (void) showNotfoundimage;
- (void) showWorkingimage;

@end

