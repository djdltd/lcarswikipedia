//
//  LCARSKeyboardView.h
//  LCARS Wikipedia
//
//  Created by Danny Draper on 21/02/2011.
//  Copyright 2011 CedeSoft Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface LCARSKeyboardView : UIView {

	IBOutlet UIImageView *imgview_blanktopleft;
	
}

@property (retain, nonatomic) UIImageView *imgview_blanktopleft;

@end
