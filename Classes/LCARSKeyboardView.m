//
//  LCARSKeyboardView.m
//  LCARS Wikipedia
//
//  Created by Danny Draper on 21/02/2011.
//  Copyright 2011 CedeSoft Ltd. All rights reserved.
//

#import "LCARSKeyboardView.h"



@implementation LCARSKeyboardView

@synthesize imgview_blanktopleft;

- (id)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code.
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code.
}
*/

- (void)dealloc {
    [super dealloc];
}


@end
