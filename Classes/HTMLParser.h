//
//  HTMLParser.h
//  LCARS Wikipedia
//
//  Created by Danny Draper on 05/02/2011.
//  Copyright 2011 CedeSoft Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ParserEngine.h"

@protocol HTMLParsingEngine

- (void)foundHTMLcontent:(NSString *)htmlcontent:(int)identifier;
- (void)parsingHTMLFinished;

@end

@interface HTMLParser : NSObject <ParsingEngine> {
	
	id _delegateobject;
	ParserEngine *_parserengine;
	int _tagcount;
	
	bool _binitialised;
}

- (void) parse:(NSString *)contents;
- (void) initialise;
- (void) setDelegate:(NSObject *)delegate;

- (void) notifyHTMLcontentfound:(NSString *)htmlcontent:(int)identifier;
- (void) notifyHTMLparsingfinished;

@end
