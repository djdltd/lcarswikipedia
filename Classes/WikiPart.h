//
//  WikiPart.h
//  LCARS Wikipedia
//
//  Created by Danny Draper on 18/02/2011.
//  Copyright 2011 CedeSoft Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface WikiPart : NSObject {
	
	bool _newline;
	int _parttype;
	bool _heading;
	NSMutableString *_contents;
	NSMutableString *_caption;
	NSMutableString *_target;
	
	bool _contentsinitialised;
	bool _captioninitialised;
	bool _targetinitialised;
	
	// May also need to include an NSImage
}

// Get methods
- (bool) hasNewline;
- (int) getParttype;
- (bool) isHeading;
- (NSString *) getContents;
- (NSString *) getCaption;
- (NSString *) getTarget;

// Set methods
- (void) setNewline:(bool)hasnewline;
- (void) setParttype:(int)parttype;
- (void) setHeading:(bool)isheading;
- (void) setContents:(NSString *)contents;
- (void) setCaption:(NSString *)caption;
- (void) setTarget:(NSString *)target;

@end
