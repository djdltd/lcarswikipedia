//
//  SmallAlpha.m
//  LCARS Calc
//
//  Created by Danny Draper on 26/08/2010.
//  Copyright 2010 CedeSoft Ltd. All rights reserved.
//

#import "SmallAlpha.h"


@implementation SmallAlpha


- (void) Initialise:(CGFloat)offx: (CGFloat)offy: (CGFloat) standardwidth: (CGFloat) iletterwidth: (CGFloat) mletterwidth: (CGFloat) wletterwidth: (CGFloat) charheight: (CGFloat) sepwidth: (CGFloat) stopwidth: (bool) containsmathchars: (bool)containspowersymbol: (NSString *) resourcename;
{
		
	_curframe = 0;
	_visible = false;
	
	_charheight = charheight;
	_rightjustification = false;
	_sepwidth = sepwidth;
	_stopwidth = stopwidth;
	CGFloat startx = offx;
	CGFloat starty = offy;
	
	CGFloat yoffset = 0.0f;
	CGFloat xoffset = 0.0f;
	
	_containsmathcharacters = containsmathchars;
	_containspowersymbol = containspowersymbol;
	
	_alphabet = [NSMutableArray new];
	_alphastring = [NSMutableString new];
	_imageresource = [NSMutableString new];
	[_imageresource setString:resourcename];
	
	_standardwidth = standardwidth;
	_iletterwidth = iletterwidth;
	_mletterwidth = mletterwidth;
	_wletterwidth = wletterwidth;
		
	UIImage *alphafontmap;
	
	alphafontmap = [UIImage imageNamed:_imageresource];
	
	
	CGRect imageRect;
	UIImage *frame1;
	
	int i = 0;
	
	/*
	 The letter i, m and w have different character widths than the
	 standard width supplied so we need to check for these explicitly
	 i = 9
	 m = 13
	 w = 23
	 
	 */
	
	// First level of alphabet characters
	bool bUsestandardwidth = true;
	
	int maxchars = 0;
	
	if (_containsmathcharacters == true) {
		maxchars = 62;
		
		if (_containspowersymbol == true) {
			maxchars = 63;
		}
		
	} else {
		maxchars = 39;
	}
	
	
	for (i=0;i<maxchars;++i)
	{
		bUsestandardwidth = true;
		imageRect.origin = CGPointMake (startx + xoffset, starty + yoffset);
		
		if (i == 9) {
			imageRect.size = CGSizeMake(_iletterwidth+_sepwidth, _charheight);
			bUsestandardwidth = false;
			xoffset+=_iletterwidth;
		} 
		
		if (i == 13) {
			imageRect.size = CGSizeMake(_mletterwidth+_sepwidth, _charheight);
			bUsestandardwidth = false;
			xoffset+=_mletterwidth;
		}
		
		if (i == 23) {
			imageRect.size = CGSizeMake(_wletterwidth+_sepwidth, _charheight);
			bUsestandardwidth = false;
			xoffset+=_wletterwidth;
		}
		
		if (bUsestandardwidth == true) {
			imageRect.size = CGSizeMake(_standardwidth+_sepwidth, _charheight);
			xoffset+=_standardwidth;
		}
		
		frame1 = [UIImage imageWithCGImage:CGImageCreateWithImageInRect([alphafontmap CGImage], imageRect)];
		[_alphabet addObject:frame1];
	}
}

- (void) setRightjustification:(bool)rightjust
{
	_rightjustification = rightjust;
}

- (void) setAlphastring:(NSString *)alphastring
{
	[_alphastring setString:alphastring];
	
}

- (NSString *) getAlphastring
{
	return _alphastring;
}

- (void) setVisible:(bool)visible
{
	_visible = visible;
}

- (void) setLocation:(CGPoint)location
{
	_location = location;
}

- (void) PaintString
{
	[self PaintString:false];
}

- (CGSize) GetTextMetrics
{
	CGSize textsize;
	
	[self PaintString:true];
	
	textsize = CGSizeMake(_stringwidth, _stringheight);
	
	return textsize;
}

- (int) GetFrameNumber:(UniChar)lookupchar
{
	if (lookupchar == ' ') {return 0;}		
	if (lookupchar == 'A') {return 1;}		
	if (lookupchar == 'B') {return 2;}
	if (lookupchar == 'C') {return 3;}
	if (lookupchar == 'D') {return 4;}		
	if (lookupchar == 'E') {return 5;}
	if (lookupchar == 'F') {return 6;}		
	if (lookupchar == 'G') {return 7;}
	if (lookupchar == 'H') {return 8;}		
	if (lookupchar == 'I') {return 9;}		
	if (lookupchar == 'J') {return 10;}		
	if (lookupchar == 'K') {return 11;}		
	if (lookupchar == 'L') {return 12;}		
	if (lookupchar == 'M') {return 13;}		
	if (lookupchar == 'N') {return 14;}		
	if (lookupchar == 'O') {return 15;}		
	if (lookupchar == 'P') {return 16;}		
	if (lookupchar == 'Q') {return 17;}		
	if (lookupchar == 'R') {return 18;}		
	if (lookupchar == 'S') {return 19;}		
	if (lookupchar == 'T') {return 20;}
	if (lookupchar == 'U') {return 21;}		
	if (lookupchar == 'V') {return 22;}		
	if (lookupchar == 'W') {return 23;}		
	if (lookupchar == 'X') {return 24;}		
	if (lookupchar == 'Y') {return 25;}		
	if (lookupchar == 'Z') {return 26;}
	
	if (lookupchar == 'a') {return 1;}		
	if (lookupchar == 'b') {return 2;}
	if (lookupchar == 'c') {return 3;}
	if (lookupchar == 'd') {return 4;}		
	if (lookupchar == 'e') {return 5;}
	if (lookupchar == 'f') {return 6;}		
	if (lookupchar == 'g') {return 7;}
	if (lookupchar == 'h') {return 8;}		
	if (lookupchar == 'i') {return 9;}		
	if (lookupchar == 'j') {return 10;}		
	if (lookupchar == 'k') {return 11;}		
	if (lookupchar == 'l') {return 12;}		
	if (lookupchar == 'm') {return 13;}
	if (lookupchar == 'n') {return 14;}		
	if (lookupchar == 'o') {return 15;}		
	if (lookupchar == 'p') {return 16;}		
	if (lookupchar == 'q') {return 17;}		
	if (lookupchar == 'r') {return 18;}		
	if (lookupchar == 's') {return 19;}		
	if (lookupchar == 't') {return 20;}
	if (lookupchar == 'u') {return 21;}		
	if (lookupchar == 'v') {return 22;}		
	if (lookupchar == 'w') {return 23;}		
	if (lookupchar == 'x') {return 24;}		
	if (lookupchar == 'y') {return 25;}		
	if (lookupchar == 'z') {return 26;}
	
	if (lookupchar == '.') {return 27;}		
	if (lookupchar == ',') {return 28;}		
	if (lookupchar == '1') {return 29;}		
	if (lookupchar == '2') {return 30;}		
	if (lookupchar == '3') {return 31;}		
	if (lookupchar == '4') {return 32;}		
	if (lookupchar == '5') {return 33;}		
	if (lookupchar == '6') {return 34;}		
	if (lookupchar == '7') {return 35;}		
	if (lookupchar == '8') {return 36;}
	if (lookupchar == '9') {return 37;}
	if (lookupchar == '0') {return 38;}

	
	if (_containsmathcharacters == true) {		
		if (lookupchar == '(') {return 39;}		
		if (lookupchar == ')') {return 40;}		
		if (lookupchar == '+') {return 41;}
		if (lookupchar == '-') {return 42;}		
		if (lookupchar == '*') {return 46;}
		if (lookupchar == '/') {return 57;}
		if (lookupchar == '=') {return 45;}
		
		if (lookupchar == '!') {return 47;}
		if (lookupchar == '@') {return 48;}
		//if (lookupchar == '£') {return 49;}
		if (lookupchar == '$') {return 50;}
		if (lookupchar == '%') {return 51;}
		if (lookupchar == '&') {return 52;}
		if (lookupchar == ':') {return 53;}
		if (lookupchar == ';') {return 54;}
		if (lookupchar == 39) {return 55;}
		if (lookupchar == '\\') {return 56;}
		if (lookupchar == '/') {return 57;}
		if (lookupchar == '?') {return 58;}
		if (lookupchar == '<') {return 59;}
		if (lookupchar == '>') {return 60;}
		if (lookupchar == '"') {return 61;}
		
		
		if (_containspowersymbol == true) {
			if (lookupchar == '^') {return 46;}
		}
	}
	
	return 0;
}

- (void) PaintString:(bool)metricsonly
{
	if (_visible == false)
	{
		return;
	}
	
	if (metricsonly == true) {
		_stringwidth = 0.0f;
		_stringheight = 0.0f;
	}
	
	int c = 0;
	
	CGFloat xoffset = 0.0f;
	CGFloat xrightoffset = 0.0f;
	CGFloat xincrement = _sepwidth;
	
	CGFloat xlocation = _location.x;
	CGFloat ylocation = _location.y;
	CGFloat currentwidth = 0.0f;
	
	for (c=0;c<[_alphastring length];++c)
	{
		UniChar singlechar = [_alphastring characterAtIndex:c];
		frame = nil;
		
		frame = [_alphabet objectAtIndex:[self GetFrameNumber:singlechar]];
		
		if (_containsmathcharacters == true) {		
			
			frame = [_alphabet objectAtIndex:[self GetFrameNumber:singlechar]];
			
			if (_containspowersymbol == true) {
				frame = [_alphabet objectAtIndex:[self GetFrameNumber:singlechar]];
			}
			
		}
		
		if (frame != nil) {
			if (_rightjustification == false) {
				if (metricsonly == false) {
					[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
				}
			}
					
		
			currentwidth = _standardwidth;
		
			if (singlechar == 'I') {currentwidth = _iletterwidth;}
			if (singlechar == 'M') {currentwidth = _mletterwidth;}
			if (singlechar == 'W') {currentwidth = _wletterwidth;}
			if (singlechar == '.') {currentwidth = _stopwidth;}
			
			if (singlechar == 'i') {currentwidth = _iletterwidth;}
			if (singlechar == 'm') {currentwidth = _mletterwidth;}
			if (singlechar == 'w') {currentwidth = _wletterwidth;}
			
		
			xoffset+=currentwidth+xincrement;
			
			if (metricsonly == true) {
				_stringwidth+=currentwidth + xincrement;
			}
			
			if (_rightjustification == true) {
				xrightoffset = xoffset;
			}
		}
	}
	
	if (_rightjustification == true) {
		xoffset = 0.0f;
		
		for (c=0;c<[_alphastring length];++c)
		{
			UniChar singlechar = [_alphastring characterAtIndex:c];
			frame = nil;
			
			frame = [_alphabet objectAtIndex:[self GetFrameNumber:singlechar]];
			
			if (_containsmathcharacters == true) {		

				frame = [_alphabet objectAtIndex:[self GetFrameNumber:singlechar]];
				
				if (_containspowersymbol == true) {
					frame = [_alphabet objectAtIndex:[self GetFrameNumber:singlechar]];
				}
			}
			
			if (frame != nil) {
				if (_rightjustification == true) {
					if (metricsonly == false) {
						[frame drawAtPoint:CGPointMake(xlocation - xrightoffset + xoffset, ylocation)];
					}
				}
				
				
				currentwidth = _standardwidth;
				
				if (singlechar == 'I') {currentwidth = _iletterwidth;}
				if (singlechar == 'M') {currentwidth = _mletterwidth;}
				if (singlechar == 'W') {currentwidth = _wletterwidth;}
				if (singlechar == '.') {currentwidth = _stopwidth;}
				
				if (singlechar == 'i') {currentwidth = _iletterwidth;}
				if (singlechar == 'm') {currentwidth = _mletterwidth;}
				if (singlechar == 'w') {currentwidth = _wletterwidth;}
				
				xoffset+=currentwidth+xincrement;
				
				if (metricsonly == true) {
					_stringwidth+=currentwidth + xincrement;
				}
			}
		}
	}
	
	if (metricsonly == true)
	{
		_stringheight = _charheight;
	}
}


- (void) Paint
{
	
}

@end
