//
//  HTMLParser.m
//  LCARS Wikipedia
//
//  Created by Danny Draper on 05/02/2011.
//  Copyright 2011 CedeSoft Ltd. All rights reserved.
//

#import "HTMLParser.h"


@implementation HTMLParser

- (void) initialise
{
	if (_binitialised != true) {
		_parserengine = [[[ParserEngine alloc] init] retain];
		[_parserengine setDelegate:self];
		
		[_parserengine addTokentag:@"<" :@">" : 100];
		
		_binitialised = true;
	}
}

- (void) setDelegate:(NSObject *)delegate
{
	_delegateobject = delegate;
}


- (void) notifyHTMLcontentfound:(NSString *)htmlcontent:(int)identifier
{
	if ([_delegateobject respondsToSelector:@selector(foundHTMLcontent::)]) {
		[_delegateobject foundHTMLcontent:htmlcontent :identifier];
	}
}

- (void) notifyHTMLparsingfinished
{
	if ([_delegateobject respondsToSelector:@selector(parsingHTMLFinished)]) {
		[_delegateobject parsingHTMLFinished];
	}
}

- (void) parsingFinished
{
	//NSLog (@"HTML Parser has finished.");
	[self notifyHTMLparsingfinished];
}

- (void) foundToken:(NSString *)tokencontents :(int)identifier
{
	//NSLog (@"");
	//NSLog (@"\n*************************************************START HTML TOKEN ID %i ***********************************************\n%@\n*************************************************END HTML TOKEN ID %i ***********************************************\n", identifier, tokencontents, identifier);
	//NSString *test;
	//test = [_parserengine leftString:@"testhead" :1];
	//NSLog (@"Test leftstring: %@", test);
	
	if (identifier == 100) { // We've been given a html tag
	
		//NSString *ook = @"\n \t\t hello there \t\n  \n\n";
		NSString *trimmedtoken = [tokencontents stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
		
		//NSLog(@"trimmed: '%@'", trimmed);
		//NSLog (@"HTML Token: %@", tokencontents);
		
		if ([[_parserengine rightString:trimmedtoken :1] isEqualToString:@"/"] == true) {
			// This is an open and close tag straight off
			//NSLog (@"HTML Complete Token: %@", trimmedtoken);
		} else {
			
			if ([[_parserengine leftString:trimmedtoken :1] isEqualToString:@"/"] == true) {
				//NSLog (@"HTML Closing Token: %@", trimmedtoken);
				
				
				
				if ([[_parserengine rightString:trimmedtoken :3] isEqualToString:@"ref"] == true) {
					_tagcount--;
					
					
				}
				
				//NSLog(@"HTML TagCount: %i", _tagcount);
				
			} else {
				//NSLog (@"HTML Opening Token: %@", trimmedtoken);
				
				
				
				if ([[_parserengine leftString:trimmedtoken :3] isEqualToString:@"ref"] == true) {
					
					
					
					_tagcount++;

				}
				
				//NSLog(@"HTML TagCount: %i", _tagcount);
				
				
				
			}
			//NSLog (@"HTML Opening or Closing token: %@: ", trimmedtoken);
		}
	} else { // We've been given a plain text tag, so decide which identifier to give it based on our previously received tag.
		
		if (_tagcount > 0) {
			//NSLog (@"HTML Tag Contents: %@", tokencontents);
		} else {
			//NSLog(@"HTML Plaintext: %@", tokencontents);
			[self notifyHTMLcontentfound:tokencontents :0];
		}
		
	}
	
	
}


- (void) parse:(NSString *)contents
{
	_tagcount = 0;
	if (_binitialised != true) {
		[self initialise];
	}
	
	[_parserengine parse:contents];
}

@end
