//
//  LCARSTextView.m
//  LCARS Wikipedia
//
//  Created by Danny Draper on 13/01/2011.
//  Copyright 2011 CedeSoft Ltd. All rights reserved.
//

#import "LCARSTextView.h"


@implementation LCARSTextView


- (id)initWithCoder:(NSCoder *)coder
{
	if ((self = [super initWithCoder:coder])) {
		
		_lcarsstring = [[[SmallAlpha alloc] init] retain];
		
		[_lcarsstring Initialise:5.0f :3.0f :14.0f :8.0f :18.0f :19.0f :33.0f : -1.0f : 8.0f: true : false: @"UCTypeAlpha.png"];
		[_lcarsstring setLocation:CGPointMake (712.0f, 390.0f)];
		[_lcarsstring setVisible:true];
		
		[_lcarsstring setLocation:CGPointMake (10.0f, 10.0f)];
		
		_lcarsstringorange = [[[SmallAlpha alloc] init] retain];
		
		[_lcarsstringorange Initialise:5.0f :3.0f :14.0f :8.0f :18.0f :19.0f :33.0f : -1.0f : 8.0f: true : false: @"UCTypeAlphaOrange.png"];
		[_lcarsstringorange setLocation:CGPointMake (712.0f, 390.0f)];
		[_lcarsstringorange setVisible:true];
		
		[_lcarsstringorange setLocation:CGPointMake (10.0f, 10.0f)];

		_lcarsstringyellow = [[[SmallAlpha alloc] init] retain];
		
		[_lcarsstringyellow Initialise:5.0f :3.0f :14.0f :8.0f :18.0f :19.0f :33.0f : -1.0f : 8.0f: true : false: @"UCTypeAlphaYellow.png"];
		[_lcarsstringyellow setLocation:CGPointMake (712.0f, 390.0f)];
		[_lcarsstringyellow setVisible:true];
		
		[_lcarsstringyellow setLocation:CGPointMake (10.0f, 10.0f)];
		
		//[_lcarsstring setAlphastring:@"THIS IS A COOL TEST "];
		
		// Test string used for counting the number of pages
		_teststring = [[[SmallAlpha alloc] init] retain];
		[_teststring Initialise:5.0f :3.0f :14.0f :8.0f :18.0f :19.0f :33.0f : -1.0f : 8.0f: true : false: @"UCTypeAlpha.png"];
		[_teststring setLocation:CGPointMake (10.0f, 10.0f)];
		[_teststring setVisible:true];
		
		_pagelabel = [[[NSMutableString alloc] init] retain];
		
		_parserengine = [[[ParserEngine alloc] init] retain];
		[_parserengine setDelegate:self];
		
		_htmlparser = [[[HTMLParser alloc] init] retain];
		[_htmlparser setDelegate:self];
		
		//_htmlparsedoutput = [[[NSMutableString alloc] init] retain];
		_htmlparsedoutput = nil;
		
		_articlesummarylist = nil;
		_articlesummarystring = nil;
		_articlesummarybackup = nil;
		_wikiparts = nil;
		_wordobjectsbackup = nil;
		_wantedxmlselection = nil;
		_redirectflag = false;
	
		_wordobjects = nil;
		
		_pagearray = nil;
		
		[_parserengine addTokentag:@"{|" :@"|}" :TEMPLATETAG];
		[_parserengine addTokentag:@"{{" :@"}}" :TEMPLATETAG];
		//[_parserengine addTokentag:@"====" :@"====" :EXTRABOLD];
		//[_parserengine addTokentag:@"===" :@"===" :HEADERTAG];
		[_parserengine addTokentag:@"==" :@"==" :SUBHEADERTAG];
		[_parserengine addTokentag:@"[[" :@"]]" :LINKTAG];		
		//[_parserengine addTokentag:@"'''" :@"'''" :BOLDTAG];
		//[_parserengine addTokentag:@"''" :@"''" :ITALICTAG];

		
		[_parserengine addReplaceable:@"&nbsp" :@" "];
		[_parserengine addReplaceable:@"ƒÏ" :@"-"];
		[_parserengine addReplaceable:@"'''" :@""];
		[_parserengine addReplaceable:@"''" :@""];
		[_parserengine addReplaceable:@";" :@""];
		[_parserengine addReplaceable:@"  " :@""];
		
		[_parserengine addPreparseReplaceable:@"====" :@"=="];
		[_parserengine addPreparseReplaceable:@"===" :@"=="];
		
		// Need to add pre-replaceables
		// Tags that get replaced before parsing.
		
		//CGSize textsize = [_lcarsstring GetTextMetrics];
		
		//NSLog (@"Text width: %f", textsize.width);
		//NSLog (@"Text height: %f", textsize.height);
		
		//self.frame = CGRectMake(10.0f, 10.0f, 250.0f, 250.0f);
		_wordsavailable = false;
		_redirecttofirstlink = false;
		
		[self configureAudioServices];
		
		NSLog (@"The LCARS text view has been initialised.");
	}
	return self;
}

- (void) configureAudioServices {
	
	
	AudioSessionInitialize (NULL, NULL,	NULL,NULL);
	
	//UInt32 sessionCategory = kAudioSessionCategory_AmbientSound;
	UInt32 sessionCategory = kAudioSessionCategory_MediaPlayback;
	AudioSessionSetProperty (kAudioSessionProperty_AudioCategory, sizeof (sessionCategory), &sessionCategory);
	
	
	UInt32 audioOverride =  true;
	AudioSessionSetProperty (kAudioSessionProperty_OverrideCategoryMixWithOthers, sizeof (audioOverride), &audioOverride);
	
	AudioSessionSetActive (true);
}

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{

	if (player != nil)
	{
		if (player == _myaudioplayerone) {
			_audioplayingone = false;
			NSLog (@"Releasing Audioplayer one...");
			[_myaudioplayerone release];
			_myaudioplayerone = nil;
			return;
		}
		
		if (player == _myaudioplayertwo) {
			_audioplayingtwo = false;
			NSLog (@"Releasing Audioplayer two...");
			[_myaudioplayertwo release];
			_myaudioplayertwo = nil;
			return;
		}
	}
}

- (void)playSinglesoundChOne:(NSString *)resourcename
{
		
	if (_audioplayingone == true) {
		if (_myaudioplayerone != nil)
		{
			[_myaudioplayerone stop];
			[_myaudioplayerone release];
		}
	}
	
	
	NSString *buttonPath = [[NSBundle mainBundle] pathForResource:resourcename ofType:@"wav" inDirectory:@"/"];
	
	if (buttonPath == nil) {
		buttonPath = [[NSBundle mainBundle] pathForResource:resourcename ofType:@"WAV" inDirectory:@"/"];
	}
	
	if (buttonPath != nil) {
		CFURLRef sndbuttonURL;
		NSURL *url = [[NSURL alloc] initFileURLWithPath:buttonPath];
		sndbuttonURL = (CFURLRef)url;
		
		_myaudioplayerone = [[AVAudioPlayer alloc] initWithContentsOfURL: url error: nil];		
		[url release];
		
		[_myaudioplayerone setVolume:1];
		[_myaudioplayerone setDelegate:self];
		[_myaudioplayerone prepareToPlay];
		[_myaudioplayerone play];	
		_audioplayingone = true;
	}	
}

- (void)playSinglesoundChTwo:(NSString *)resourcename
{
	if (_audioplayingtwo == true) {
		if (_myaudioplayertwo != nil)
		{
			[_myaudioplayertwo stop];
			[_myaudioplayertwo release];
		}
	}
	
	
	NSString *buttonPath = [[NSBundle mainBundle] pathForResource:resourcename ofType:@"wav" inDirectory:@"/"];
	
	if (buttonPath == nil) {
		buttonPath = [[NSBundle mainBundle] pathForResource:resourcename ofType:@"WAV" inDirectory:@"/"];
	}
	
	if (buttonPath != nil) {
		CFURLRef sndbuttonURL;
		NSURL *url = [[NSURL alloc] initFileURLWithPath:buttonPath];
		sndbuttonURL = (CFURLRef)url;
		
		_myaudioplayertwo = [[AVAudioPlayer alloc] initWithContentsOfURL: url error: nil];		
		[url release];
		
		[_myaudioplayertwo setVolume:1];
		[_myaudioplayertwo setDelegate:self];
		[_myaudioplayertwo prepareToPlay];
		[_myaudioplayertwo play];	
		_audioplayingtwo = true;
	}
}

- (void) setDelegate:(id)delegate
{
	_delegateobject = delegate;
}

- (void)setViewframe:(CGRect)viewframe
{
	self.frame = viewframe;
	
	_currentrect = viewframe;
}

- (id)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code.
		NSLog(@"Init with frame has been called.");
    }
    return self;
}

- (void) startDirecttextrender:(NSString *)stringtorender
{
	if (_wordobjects != nil) {
		// Clean up any previously used word objects
		[_wordobjects release];
		_wordobjects = nil;
	}
	
	NSLog(@"Start Text Rendering has been called.");
	
	// The first thing to do is split the string up into words
	// so we can then call the invalidate and get this displayed with appropriate word wrapping
	
	//[self createWordobjects:stringtorender];
	[self resetParser];
	
	[self beginWikiparsing:stringtorender];
	
	//[self setNeedsDisplay];
	
	//NSLog (@"Number of words downloaded: %i", [_wordlist count]);
}

// This source file is organised (sort of) by by order of things that happen
// So the first method is the entry point to the parsing.

- (void) startTextrendering:(NSString *)stringtorender
{
	
	if (_wordobjects != nil) {
		// Clean up any previously used word objects
		[_wordobjects release];
		_wordobjects = nil;
	}
	
	NSLog(@"Start Text Rendering has been called.");
	
	// The first thing to do is split the string up into words
	// so we can then call the invalidate and get this displayed with appropriate word wrapping
	
	//[self createWordobjects:stringtorender];
	[self resetParser];
	
	NSLog (@"startTextrendering: stringtorender length: %i", [stringtorender length]);
	
	[self parseXML:stringtorender];
	//[self setNeedsDisplay];
	
	//NSLog (@"Number of words downloaded: %i", [_wordlist count]);
}

// Next comes the XML parsing....

- (void) parseXML:(NSString *)xmldata
{
	_wikixmlfound = false;
	_wantedxmlchars = false;
	_xmlparser = [[NSXMLParser alloc] initWithData:[xmldata dataUsingEncoding:NSUTF8StringEncoding]];
	
	if (_wantedxmlselection != nil) {
		[_wantedxmlselection release];
		_wantedxmlselection = nil;
	}
	
	if (_wantedxmlselection == nil) {
		_wantedxmlselection = [[[NSMutableString alloc] init] retain];
	}
	
	[_xmlparser setDelegate:self];
	[_xmlparser parse];
}

// These callbacks get triggered by the XML parser...

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
	
	NSLog (@"An element was started");
	NSLog(@"An element was started: %@", elementName);
	
	if ([elementName isEqualToString:@"rev"] == true) {
		_wantedxmlchars = true;
	}
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
	
	if (_wantedxmlchars == true) {
		[_wantedxmlselection appendString:string];
	}
	//NSLog(@"Character found: %@", string);
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
	
	
	NSLog (@"An element was ended.");
	NSLog (@"An element was ended: %@", elementName);
	
	if ([elementName isEqualToString:@"rev"] == true) {
		NSLog (@"Found rev XML tag!");
		
		_wantedxmlchars = false;
		
		if ([_wantedxmlselection length] > 0) {
			
		} else {
			NSLog (@"Warning! Wanted XML Selection is 0!");
		}
		
		if (_htmlparsedoutput == nil) {
			// Allocate some memory for the html parsed output
			_htmlparsedoutput = [[[NSMutableString alloc] init] retain];
		}
		
		//NSLog (@"Wanted XML Selection: %@", _wantedxmlselection);
		_wikixmlfound = true;
		// Kick off the next stage of parsing - html tag parsing.
		
		NSLog (@"didEndElement: _wantedxmlselection length: %i", [_wantedxmlselection length]);
		
		[_htmlparser parse:_wantedxmlselection];
	}
}

- (void)parserDidEndDocument:(NSXMLParser *)parser
{
	if (_wikixmlfound == false) {
		// The XML parsing was complete but no wiki xml data was found
		// We need to display an appropriate message.
		if ([_delegateobject respondsToSelector:@selector(wikiarticleNotfound)]) {
			[_delegateobject wikiarticleNotfound];
		}
	}
}

// These are the HTML callbacks
- (void) foundHTMLcontent:(NSString *)htmlcontent :(int)identifier
{
	NSLog (@"HTML content found.");
	[_htmlparsedoutput appendString:htmlcontent];
}

- (void) parsingHTMLFinished
{
	NSLog (@"HTML parsing finished.");
	
	if (_wantedxmlselection != nil) {
		NSLog (@"Releasing _wantedxmlselection.");
		[_wantedxmlselection release];
	}
	
	NSLog (@"foundHTMLcontent: _htmlparsedoutput length: %i", [_htmlparsedoutput length]);
	
	//NSLog (@"HTML Contents: %@", _htmlparsedoutput);
	
	if ([_htmlparsedoutput length] < 10) {
		NSLog (@"WARNING!! HTML PARSED OUTPUT LENGTH WAS BELOW 10! Investigate!");
	}
	
	[self beginWikiparsing:_htmlparsedoutput];
}

// Next is the wiki parsing...
- (void) beginWikiparsing:(NSString *)wikimarkupstring
{
	[self playSinglesoundChOne:@"AbouttoDisplay"];
	
	_redirectflag = false;
	
	if (_wikiparts == nil) {
		_wikiparts = [[[NSMutableArray alloc] init] retain];
	}
	
	//plaintext{{command{{extraaction}}text}}moreplain
	NSLog (@"About to parse...");
	NSLog (@"beginWikiparsing: wikimarkupstring length: %i", [wikimarkupstring length]);
	
	//NSLog (@"Wiki markup string: %@", wikimarkupstring);
	
	if (wikimarkupstring != nil)
	{
		if ([wikimarkupstring length] > 2) {
			[_parserengine parse:wikimarkupstring];
		} else {
			if ([_delegateobject respondsToSelector:@selector(wikiarticleNotfound)]) {
				[_delegateobject wikiarticleNotfound];
			}
		}
	} else {
		if ([_delegateobject respondsToSelector:@selector(wikiarticleNotfound)]) {
			[_delegateobject wikiarticleNotfound];
		}
	}
	
	
}



- (void) foundToken:(NSString*)tokencontents:(int)identifier
{
	
	TagType *returnedtag;
	
	//returnedtag = [_parserengine analyseTag:LINKTAG :@"something"];
	
	//NSLog (@"returned tag contents: %@", [returnedtag getCaption]);
	
	
	
	//NSLog (@"\n*************************************************START TOKEN ID %i ***********************************************\n%@\n*************************************************END TOKEN ID %i ***********************************************\n", identifier, tokencontents, identifier);
	//NSLog (@"%@", tokencontents);
	//NSLog (@"\n*************************************************END TOKEN ID %i ***********************************************\n", identifier);
		   
	if (identifier == PLAINTEXT) {
		
		//[_wikiparsedoutput appendString:tokencontents];
		if ([tokencontents length] > 0) {
		
			WikiPart *wikipart = [[WikiPart alloc] init];
			
			[wikipart setParttype:PLAINTEXT];
			[wikipart setContents:tokencontents];
			
			if ([_parserengine containsString:tokencontents:@"\n"]) {
				[wikipart setNewline:true];
				//NSLog (@"CONTAINS NEW LINE: YES: %@", tokencontents);
			} else {
				//NSLog (@"CONTAINS NEW LINE: NO: %@", tokencontents);
				[wikipart setNewline:false];
			}
			
			if ([_parserengine containsString:tokencontents :@"#REDIRECT"] == true) {
				_redirectflag = true;
				_redirecttofirstlink = true;
			}
			
			//NSLog (@"PlainText: %@", tokencontents);
			
			[_wikiparts addObject:wikipart];
		}
	}
	
	if (identifier == HEADERTAG) {
		//[_wikiparsedoutput appendString:tokencontents];
		
		WikiPart *wikipart = [[WikiPart alloc] init];
		
		[wikipart setParttype:HEADERTAG];
		[wikipart setContents:tokencontents];
		
		[_wikiparts addObject:wikipart];
		
	}
	
	if (identifier == SUBHEADERTAG) {
		//[_wikiparsedoutput appendString:tokencontents];
		
		WikiPart *wikipart = [[WikiPart alloc] init];
		
		[wikipart setParttype:SUBHEADERTAG];
		[wikipart setContents:tokencontents];
		
		[_wikiparts addObject:wikipart];
	}
	
	if (identifier == ITALICTAG) {
		//[_wikiparsedoutput appendString:tokencontents];
	}
	
	if (identifier == BOLDTAG) {
		//[_wikiparsedoutput appendString:tokencontents];
	}
	
	if (identifier == EXTRABOLD) {
		//[_wikiparsedoutput appendString:tokencontents];
	}
	
	if (identifier == LINKTAG) {
		//[_wikiparsedoutput appendString:@"\n"];
		//[_wikiparsedoutput appendString:tokencontents];
		
		//NSLog (@"About to analyse LINK TAG: %@", tokencontents);
		
		returnedtag = [_parserengine analyseTag:LINKTAG :tokencontents];
		
		if ([returnedtag getType] == FILETAG) {
			//NSLog (@"File Tag Found: %@", [returnedtag getTarget]);
			
			WikiPart *wikipart = [[WikiPart alloc] init];
			
			[wikipart setParttype:FILETAG];
			[wikipart setTarget:[returnedtag getTarget]];
			
			[_wikiparts addObject:wikipart];
			
		}
		
		if ([returnedtag getType] == WIKILINKTAG) {
			//NSLog (@"Wiki Link Tag: %@", [returnedtag getCaption]);
			//[_wikiparsedoutput appendString:[returnedtag getCaption]];
			
			WikiPart *wikipart = [[WikiPart alloc] init];
			
			[wikipart setParttype:WIKILINKTAG];
			[wikipart setCaption:[returnedtag getCaption]];
			[wikipart setTarget:[returnedtag getTarget]];
			
			[_wikiparts addObject:wikipart];
			
			if (_redirecttofirstlink == true) {
				NSLog (@"We need to redirect to: %@", [returnedtag getTarget]);
				_redirecttofirstlink = false;
				[self sendRedirectrequest:[returnedtag getTarget]];
			}
		}
		
		[returnedtag release];
	}
	
	//NSLog (@"Token Found was called: %@ with ID: %i", tokencontents, identifier);
	//if (identifier == 0) {
		//NSLog (@"%@", tokencontents);
	//}
}

- (void)clearDisplay
{
	[self setNeedsDisplay];
}

- (void)parsingFinished
{
	_currentpage = 1;
	
	//NSLog (@"Wiki Parsed Output: %@", _wikiparsedoutput);
	NSLog (@"Parser has finished.");
	
	if (_htmlparsedoutput != nil) {
		NSLog (@"Releasing _htmlparsedoutput...");
		[_htmlparsedoutput release];
		_htmlparsedoutput = nil;
	}
	
	if (_wordobjects != nil) {
		[_wordobjects release];
		_wordobjects = nil;
	}
	
	// Allocate memory for the word objects
	if (_wordobjects == nil) {
		_wordobjects = [[[NSMutableArray alloc] init] retain];
	}
	
	NSLog (@"Number of wikiparts: %i", [_wikiparts count]);
	
	// Create the word objects now that we have an array of wiki parts
	int w = 0;
	int numwantedparts = 0;
	
	
	for (w=0;w<[_wikiparts count];w++) {
		
		WikiPart *currentwikipart = [_wikiparts objectAtIndex:w];
		
		if ([currentwikipart getParttype] == SUBHEADERTAG) {
			[self createWordobjects:[currentwikipart getContents]:Yellow:SUBHEADERTAG:[currentwikipart getContents]:[currentwikipart hasNewline]];
			numwantedparts++;
		}
		
		if ([currentwikipart getParttype] == PLAINTEXT) {
			[self createWordobjects:[currentwikipart getContents]:Blue:PlainText:@"":[currentwikipart hasNewline]];
			numwantedparts++;
		}		
		
		if ([currentwikipart getParttype] == WIKILINKTAG) {
			[self createWordobjects:[currentwikipart getCaption]:Orange:WikiLink:[currentwikipart getTarget]:[currentwikipart hasNewline]];
			numwantedparts++;
		}
	}
	
	
	// Release the wikiparts memory
	if (_wikiparts != nil) {
		[_wikiparts release];
		_wikiparts = nil;
	}
	
	CGRect windowrect = self.frame;
	
	float windowwidth = windowrect.size.width;
	float windowheight = windowrect.size.height;
	
	// Allocate some memory for the list of articles
	if (_articlesummarylist != nil) {
		[_articlesummarylist release];
		_articlesummarylist = nil;
	}
	
	if (_articlesummarylist == nil) {
		_articlesummarylist = [[[NSMutableArray alloc] init] retain];
	}
	
	
	_numpages = [self calculateNumpages:10.0f :5.0f :windowwidth :windowheight];
	
	NSLog (@"Number of pages: %i", _numpages);
	
	if (_articlesummarystring != nil) {
		[_articlesummarystring release];
		_articlesummarystring = nil;
	}
	
	if (_articlesummarystring == nil) {
		_articlesummarystring = [[[NSMutableString alloc] init] retain];
	}
	
	[self buildArticlesummarymarkup];
	
	//[_pagelabel setString:@"1 of 241"];
	
	// Set the typing text word limit
	//_ttwordlimit = 2;

	// Tell our parent view that we are ready to display so hide any waiting messages.
	if ([_delegateobject respondsToSelector:@selector(displayReady)]) {
		[_delegateobject displayReady];
	}
	
	if (_redirectflag == true) {
		if ([_delegateobject respondsToSelector:@selector(redirectRequestprocessing)]) {
			[_delegateobject redirectRequestprocessing];
		}	
	}
	
	[self DisplayPage:_currentpage];
	
}

- (void)displayArticlesummary
{
	// Go through the current article wiki parts and back them up
	if (_wordobjectsbackup != nil) {
		[_wordobjectsbackup release];
		_wordobjectsbackup = nil;
	}
	
	if (_wordobjectsbackup == nil) {
		_wordobjectsbackup = [[[NSMutableArray alloc] init] retain];
	}
	
	int w = 0;
	SingleWord *currentword;
	
	for (w=0;w<[_wordobjects count];w++) {
		currentword = [_wordobjects objectAtIndex:w];
		[_wordobjectsbackup addObject:currentword]; // Transfer this part
	}
	
	// Now get rid of the original.
	if (_wordobjects != nil) {
		[_wordobjects release];
		_wordobjects = nil;
	}
	
	// Now start parsing the summary
	if (_articlesummarystring != nil) {
		
		// First we need to backup the article summary string
		
		if (_articlesummarybackup != nil) {
			[_articlesummarybackup release];
			_articlesummarybackup = nil;
		}
		
		if (_articlesummarybackup == nil) {
			_articlesummarybackup = [[[NSMutableString alloc] init] retain];
		}
		
		[_articlesummarybackup setString:_articlesummarystring];
		
		//Backup the current page number we were viewing
		_currentpagebackup = _currentpage;
		
		[self beginWikiparsing:_articlesummarystring];
	} else {
		NSLog (@"WARNING WARNING!! ArticleSummaryString was nil when asked to display!");
	}
}

- (void)restoreArticlecontent
{
	// Go through the wiki parts backup and restore it
	if (_wordobjectsbackup != nil) {
	
		if (_wordobjects != nil) {
			[_wordobjects release];
			_wordobjects = nil;
		}
		
		if (_wordobjects == nil) {
			_wordobjects = [[[NSMutableArray alloc] init] retain];
		}
		
		int b = 0;
		int numwordsrestored = 0;
		SingleWord *restoredword;

		if ([_wordobjectsbackup count] == 0) {
			NSLog (@"WARNING! Wordobjects backup has 0 objects!");
		}
		
		for (b=0;b<[_wordobjectsbackup count];b++) {
			restoredword = [_wordobjectsbackup objectAtIndex:b];
			[_wordobjects addObject:restoredword]; // Restore this part
			numwordsrestored++;
		}
		
		if (_wordobjectsbackup != nil) {
			[_wordobjectsbackup release];
			_wordobjectsbackup = nil;
		}
		
		NSLog (@"Restored %i words.", numwordsrestored);
		
		// Now we need to restore the article summary that was backed up
		if (_articlesummarybackup != nil) {
			
			if (_articlesummarystring != nil) {
				[_articlesummarystring release];
				_articlesummarystring = nil;
			}
			
			if (_articlesummarystring == nil) {
				_articlesummarystring = [[[NSMutableString alloc] init] retain];
			}
			
			
			[_articlesummarystring setString:_articlesummarybackup]; // Restore the article summary
			
			
			if (_articlesummarybackup != nil) {
				[_articlesummarybackup release];
				_articlesummarybackup = nil;
			}
			
		}
		
		CGRect windowrect = self.frame;
		
		float windowwidth = windowrect.size.width;
		float windowheight = windowrect.size.height;
		
		_numpages = [self calculateNumpages:10.0f :5.0f :windowwidth :windowheight];
		
		NSLog (@"Number of pages: %i", _numpages);

		_currentpage = _currentpagebackup; // Restore the current page from backup
		
		// Tell our parent view that we are ready to display so hide any waiting messages.
		if ([_delegateobject respondsToSelector:@selector(displayReady)]) {
			[_delegateobject displayReady];
		}
		


		if ([_delegateobject respondsToSelector:@selector(resetSummaryshown)]) {
			[_delegateobject resetSummaryshown];
		}
		
		// Once we are restored we can call parsingFinished again
		[self DisplayPage:_currentpage];
		
	} else {
		NSLog (@"WARNING WARNING!! Word objects backup was nil when asked to restore!");
	}
}

- (void)buildArticlesummarymarkup
{
	// This function goes through the article summary list and constructs the appropriate wiki markup
	// to represent that article summary
	
	if (_articlesummarystring != nil)
	{
		int i = 0;
		
		for (i=0;i<[_articlesummarylist count];i++) {
			SingleWord *currentsummary = [_articlesummarylist objectAtIndex:i];
			
			NSString *summaryname = [currentsummary getWordtext];
			int pagenumber = [currentsummary getReference];
			
			[_articlesummarystring appendFormat:@"[[INT2761893726:%i|%@]]\n", pagenumber, summaryname];
		}
		
		NSLog (@"Article summary string: %@", _articlesummarystring);
	}
	
}

- (bool)doesArticlereferenceexist:(NSString *)summaryname
{
	if (_articlesummarylist != nil) {
		
		SingleWord *currentwordobject;
		
		int i = 0;
		
		for (i=0;i<[_articlesummarylist count];i++) {			
			currentwordobject = [_articlesummarylist objectAtIndex:i];
			
			if ([[[currentwordobject getWordtext] uppercaseString] isEqualToString:[summaryname uppercaseString]] == true) {
				return true;
			}
		}
		
		return false;
	}
	
	return false;
}

- (void)addArticlereference:(NSString *)summaryname:(int)pagenumber
{
	if (_articlesummarylist != nil) {
		
		if ([self doesArticlereferenceexist:summaryname] == false) {
		
			SingleWord *currentobject = [[SingleWord alloc] init];
			
			[currentobject setWordtext:summaryname];
			[currentobject setReference:pagenumber];
			
			[_articlesummarylist addObject:currentobject];
			
			NSLog (@"Article summary reference added, name: %@, pagenumber: %i", summaryname, pagenumber);
			
		}
	}
}

- (NSString *) getPagelabel
{
	[_pagelabel setString:[NSString stringWithFormat:@"%i of %i", _currentpage, _numpages]];
	
	return _pagelabel;
}

- (void) DisplayPage:(int)pagenumber
{
	[self setNeedsDisplay];
	
	if (_teletypeTimer != nil) {
		[_teletypeTimer invalidate];
		_teletypeTimer = nil;
	}
	
	_ttcurrentword = 0;
	
	if (_pagearray != nil) {
		[_pagearray release];
		_pagearray = nil;
	}
	
	if (_pagearray == nil) {
		_pagearray = [[[NSMutableArray alloc] init] retain];
	}
	
	[self preparePage:pagenumber];
	
	
	if ([_wordobjects count] == 0) {
		// Display the not found error
		if ([_delegateobject respondsToSelector:@selector(wikiarticleNotfound)]) {
			[_delegateobject wikiarticleNotfound];
		}
	} else {
	
		// Start the timer to perform the teletyping effect
		_teletypeTimer = [NSTimer scheduledTimerWithTimeInterval:(NSTimeInterval)0.02 target:self selector:@selector(triggerTeletype:) userInfo:nil repeats:TRUE];
	}
}

- (void) NextPage
{
	if (_currentpage < _numpages) {
		_currentpage++;
		
		[self DisplayPage:_currentpage];
	}
}

- (void) PrevPage
{
	if (_currentpage > 1) {
		_currentpage--;
		
		[self DisplayPage:_currentpage];
	}
}

- (void) createWordobjects:(NSString *)stringtorender:(LCARSColor)wordcolor:(WordType)wordtype:(NSString *)target:(bool)hasnewline
{
	// Split the huge giant string by space into individual words
	// and put these words into an array of strings
	_wordlist = [[stringtorender componentsSeparatedByString:@" "] retain];
	
	_wordsavailable = true;
	
	// Now encapsulate each word into a word object
	// and create word objects for each word in the word array created above
	SingleWord *currentwordobject;
	
	int w = 0;
	
	
	
	for (w=0;w<[_wordlist count];w++) {
		
		NSString *curword = [_wordlist objectAtIndex:w];
		
		curword = [curword stringByReplacingOccurrencesOfString:@" " withString:@""];
		
		if ([curword length] > 0) {
			
			currentwordobject = [[SingleWord alloc] init];
			[currentwordobject setWordtext:[_wordlist objectAtIndex:w]];
			[currentwordobject setColor:wordcolor];
			[currentwordobject setType:wordtype];
			
			if (wordtype == WikiLink) {
				[currentwordobject setTarget:target];
			}
			
			if (wordtype == SUBHEADERTAG) {
				[currentwordobject setTarget:target];
			}
										   
			
			if (w == ([_wordlist count] - 1)) {
				// We are on the last word
				if (hasnewline == true) {
					[currentwordobject setNewline:true];
				}
			}
			
			if (w==0 && wordtype == SUBHEADERTAG) {
				[currentwordobject setNewline:true];
				
			} 
			
			if (w > 0 && wordtype == SUBHEADERTAG) {
				[currentwordobject setNewline:false];
			}
			
			if (w == ([_wordlist count] -1) && wordtype == SUBHEADERTAG)
			{
				[currentwordobject setLastword:true];
			}
			
			[_wordobjects addObject:currentwordobject];
		}
	}
	
	[_wordlist release];
}

- (bool) containsString:(NSString *)contents :(NSString *)searchingfor
{
	if ([contents rangeOfString:searchingfor].location != NSNotFound) {
		return true;
	} else {
		return false;
	}
}


- (void) sendRedirectrequest:(NSString *)target
{
	if ([self containsString:target :@"INT2761893726"] == true) {
		// This is an internal page redirect so we need to treat it differently
		
		// Now get the page number we need to redirect to
		NSArray *list = [target componentsSeparatedByString:@":"];
		
		if ([list count] > 0) {
		
			NSString *pagenumber = [list objectAtIndex:1];
			
			int ipagenumber = [pagenumber intValue];
			
			_currentpagebackup = ipagenumber;
			
			[self restoreArticlecontent]; // Restore the article content based on the backed up page number we've supplied
			
		} else {
			if ([_delegateobject respondsToSelector:@selector(requestRedirect:)]) {
				[_delegateobject requestRedirect:target];				
			}
		}
		
	} else {
		if ([_delegateobject respondsToSelector:@selector(requestRedirect:)]) {
			
			[_delegateobject requestRedirect:target];
			
		}
	}
	
	
}


- (void) triggerTeletype:(id)sender
{
	//NSLog(@"Timer triggered.");
	
	//_ttwordlimit+=2;
	
	
	if (_ttcurrentword < [_pagearray count]) {
	
		
		
		SingleWord *curpageword = [_pagearray objectAtIndex:_ttcurrentword];
		
		CGRect wordrect = [curpageword getWordrect];
		
		_lastwordrect = wordrect;
		
		//NSLog (@"Triggered Wordrect x: %f, y: %f, width: %f, height: %f", wordrect.origin.x, wordrect.origin.y, wordrect.size.width, wordrect.size.height);
		
		CGRect newrect = CGRectMake(wordrect.origin.x, wordrect.origin.y, wordrect.size.width, wordrect.size.height);
		
		[self setNeedsDisplayInRect:newrect];
		
		//[self setNeedsDisplay];
		
		if (_ttcurrentword % 8 == 1) {
		
			[self playSinglesoundChTwo:@"clockbuttonhighrep"];
		}
		
		
		
		//_ttcurrentword++;
		
	} else {
		[_teletypeTimer invalidate];		
		_teletypeTimer = nil;
		NSLog (@"New teletyping stopped - we've printed the page already.");
	}
	
	
	//[self setNeedsDisplay];
}

- (void) resetParser
{
	_redirecttofirstlink = false;
	_wordsavailable = false;
	_wantedxmlchars = false;
	_wordlist = nil;
	//[_wantedxmlselection setString:@""];
	
	[self setNeedsDisplay];
}

- (int)calculateNumpages:(float)startcursorx :(float)startcursory :(float)viewwidth :(float)viewheight
{
	//float cursorx = startcursorx;
	//float cursory = startcursory;
	
	int numpages = 1;
	
	
	/*
	if (_wordsavailable == true) {
		
		for (int a=0;a<[_wordobjects count];a++) {
			
			SingleWord *currentwordobject = [_wordobjects objectAtIndex:a];
			
			NSString *wordstring = [currentwordobject getWordtext];
			
			if (currentwordobject != nil && wordstring != nil) {
				[_teststring setAlphastring:wordstring];
				CGSize wordsize = [_teststring GetTextMetrics];
				
				if (wordsize.width < (viewwidth - cursorx)) {
					
					if ([currentwordobject getType] == SUBHEADERTAG) {
						NSString *header = [currentwordobject getTarget];
						
						//NSLog (@"Header: %@, On Page: %i", header, numpages);
						
						[self addArticlereference:header :numpages];
					}
					
					cursorx += wordsize.width;
				} else {
					
					if (wordsize.height < ((viewheight-18.0f) - (cursory+wordsize.height+3.0f))) {
						cursorx = startcursorx;
						cursory += wordsize.height+3.0f;
						cursorx += wordsize.width;
						
						if ([currentwordobject getType] == SUBHEADERTAG) {
							NSString *header = [currentwordobject getTarget];
							
							//NSLog (@"Header: %@, On Page: %i", header, numpages);
							[self addArticlereference:header :numpages];
						}
						
					} else {
						
						// We've gone to a new page - increase the number of pages
						// and reset the cursor back to the beginning.
						cursorx = startcursorx;
						cursory = startcursory;
						numpages++;
						cursorx += wordsize.width;
						
						
						if ([currentwordobject getType] == SUBHEADERTAG) {
							NSString *header = [currentwordobject getTarget];
							
							//NSLog (@"Header: %@, On Page: %i", header, numpages);
							[self addArticlereference:header :numpages];
						}
					}
					
					
				}
				
				
				
				cursorx += 10.0f;
				
				if ([currentwordobject getNewline] == true && [currentwordobject getType] != SUBHEADERTAG) {
					cursorx = 10.0f;
					cursory += wordsize.height+3.0f;
					
				}
				
			}
			
		}		
	}
	
	*/
	
	if (_wordsavailable == true) {
		
		//int len = [_wordobjects count];
		
		float cursorx = startcursorx;
		float cursory = startcursory;
		
		
		for (int a=0;a<[_wordobjects count];a++) {
			
			SingleWord *currentwordobject = [_wordobjects objectAtIndex:a];
			
			
			//NSLog (@"current Word: %@", currentword);
			NSString *wordstring = [currentwordobject getWordtext];
			
			if (currentwordobject != nil && wordstring != nil) {
				
				[_lcarsstring setAlphastring:wordstring];
				CGSize wordsize = [_lcarsstring GetTextMetrics];
				
				if ([currentwordobject getType] == SUBHEADERTAG) {
					
					NSString *header = [currentwordobject getTarget];
					
					//NSLog (@"Header: %@, On Page: %i", header, numpages);
					
					[self addArticlereference:header :numpages];
					
					
					if ([currentwordobject getNewline] == true) {
						cursorx = 10.0f;
						cursory += wordsize.height+3.0f;
					}
				}
				
				if (wordsize.width < (viewwidth - cursorx)) { // will the word fit on the line
					
					if (wordsize.height < ((viewheight) - (cursory+wordsize.height+3.0f))) { // If we can still fit within the current page given any line breaks that might have been added.
												
						cursorx += wordsize.width;
					} else {
						
						cursorx = 10.0f;
						cursory = 5.0f;
						numpages++;																
						
						cursorx += wordsize.width;
						
					}
																		
				} else {
					
					
					if (wordsize.height < ((viewheight) - (cursory+wordsize.height+3.0f))) { // If we can fit within the current page
						
						// Make a new line
						cursorx = 10.0f;
						cursory += wordsize.height+3.0f;						
						
						cursorx += wordsize.width;
						
					} else { // newpage needed
						
						// Clear the page, then make a new line
						cursorx = 10.0f;
						cursory = 5.0f;
						numpages++;					
						
						cursorx += wordsize.width;
						
					}
				}
				
				
				// If the next word is a single S then don't add a space to the end of this word
				if ((a+1) < [_wordobjects count]) {
					SingleWord *nextwordobject = [_wordobjects objectAtIndex:(a+1)];						
					NSString *nextstring = [nextwordobject getWordtext];
					
					NSString *trimmedString = [nextstring stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
					
					bool addspace = true;
					
					if ([[trimmedString uppercaseString] isEqualToString:@"S"] == true) {
						addspace = false;
					}
					
					if ([[trimmedString uppercaseString] isEqualToString:@","] == true) {
						addspace = false;
					}
					
					if (addspace == true) {
						cursorx += 10.0f;
					}
					
				} else {
					cursorx += 10.0f;
				} 
				
				if ([currentwordobject getNewline] == true && [currentwordobject getType] != SUBHEADERTAG) {
					cursorx = 10.0f;
					cursory += wordsize.height+3.0f;
				}
				
				if ([currentwordobject getType] == SUBHEADERTAG && [currentwordobject getLastword] == true) {
					cursorx = 10.0f;
					cursory += wordsize.height+3.0f;
				}
				
				//cursorx += 10.0f;
			}	
			
			
		} // for loop
		
	} // wordsavailable	
	
	
	 
	return numpages;
}

- (void)preparePage:(int)wantedpage
{
	[_pagearray removeAllObjects]; // Clear the page
	
	
	CGRect windowrect = self.frame;
	
	//NSLog(@"window window left: %f", windowrect.origin.x);
	//NSLog(@"window window right: %f", windowrect.origin.y);
	//NSLog(@"window width: %f", windowrect.size.width);
	//NSLog(@"window window height: %f", windowrect.size.height);
	
	// Wanted page should start from 1 - not 0. 
	int currentpage = 1;
	
	if (_wordsavailable == true) {
		
		//int len = [_wordobjects count];
		
		float cursorx = 10.0f;
		float cursory = 5.0f;
		
		
		for (int a=0;a<[_wordobjects count];a++) {
			
			SingleWord *currentwordobject = [_wordobjects objectAtIndex:a];
			
		
			
			//NSLog (@"current Word: %@", currentword);
			NSString *wordstring = [currentwordobject getWordtext];
			
			if (currentwordobject != nil && wordstring != nil) {
				
				[_lcarsstring setAlphastring:wordstring];
				CGSize wordsize = [_lcarsstring GetTextMetrics];
				
				if ([currentwordobject getType] == SUBHEADERTAG) {
					if ([currentwordobject getNewline] == true) {
						cursorx = 10.0f;
						cursory += wordsize.height+3.0f;
					}
				}
				
				if (wordsize.width < (windowrect.size.width - cursorx)) { // will the word fit on the line
					
					if (wordsize.height < ((windowrect.size.height) - (cursory+wordsize.height+3.0f))) { // If we can still fit within the current page given any line breaks that might have been added.
					
						if (wantedpage == currentpage) { // are we on the page we want																		
							
							CGRect wordrect = CGRectMake(cursorx, cursory, wordsize.width, wordsize.height);
							
							[currentwordobject setWordrect:wordrect];
							[_pagearray addObject:currentwordobject];
							
							//[_lcarsstring setLocation:CGPointMake(cursorx, cursory)];							
							//[_lcarsstring PaintString];
						}
						
						cursorx += wordsize.width;
					} else {
					
						cursorx = 10.0f;
						cursory = 5.0f;
						currentpage++;
						
						if (wantedpage == currentpage) {
							
							CGRect wordrect = CGRectMake(cursorx, cursory, wordsize.width, wordsize.height);
							
							[currentwordobject setWordrect:wordrect];
							[_pagearray addObject:currentwordobject];
							
							//[_lcarsstring setLocation:CGPointMake(cursorx, cursory)];
							//[_lcarsstring PaintString];
						}
						
						if (currentpage > wantedpage) {
							//NSLog (@"Stopped drawing words at word number %i", a);
							break; // Exit the for loop.
						}
						
						cursorx += wordsize.width;
						
					}
					
					

					
				} else {
					
					
					if (wordsize.height < ((windowrect.size.height) - (cursory+wordsize.height+3.0f))) { // If we can fit within the current page
						
						// Make a new line
						cursorx = 10.0f;
						cursory += wordsize.height+3.0f;
						
						if (wantedpage == currentpage) {												
							
							CGRect wordrect = CGRectMake(cursorx, cursory, wordsize.width, wordsize.height);
							
							[currentwordobject setWordrect:wordrect];
							[_pagearray addObject:currentwordobject];
														
							//[_lcarsstring setLocation:CGPointMake(cursorx, cursory)];
							//[_lcarsstring PaintString];
						}
						
						cursorx += wordsize.width;
						
					} else { // newpage needed
						
						// Clear the page, then make a new line
						cursorx = 10.0f;
						cursory = 5.0f;
						currentpage++;
						
						if (wantedpage == currentpage) {
							
							CGRect wordrect = CGRectMake(cursorx, cursory, wordsize.width, wordsize.height);
							
							[currentwordobject setWordrect:wordrect];
							[_pagearray addObject:currentwordobject];
							
							//[_lcarsstring setLocation:CGPointMake(cursorx, cursory)];
							//[_lcarsstring PaintString];
						}
						
						if (currentpage > wantedpage) {
							//NSLog (@"Stopped drawing words at word number %i", a);
							break; // Exit the for loop.
						}
						
						cursorx += wordsize.width;
						
					}
				}
				
				
				// If the next word is a single S then don't add a space to the end of this word
				if ((a+1) < [_wordobjects count]) {
					SingleWord *nextwordobject = [_wordobjects objectAtIndex:(a+1)];						
					NSString *nextstring = [nextwordobject getWordtext];
					
					NSString *trimmedString = [nextstring stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
					
					bool addspace = true;
					
					if ([[trimmedString uppercaseString] isEqualToString:@"S"] == true) {
						addspace = false;
					}
					
					if ([[trimmedString uppercaseString] isEqualToString:@","] == true) {
						addspace = false;
					}
					
					if (addspace == true) {
						cursorx += 10.0f;
					}
					
				} else {
					cursorx += 10.0f;
				} 
				
				if ([currentwordobject getNewline] == true && [currentwordobject getType] != SUBHEADERTAG) {
					cursorx = 10.0f;
					cursory += wordsize.height+3.0f;
				}
				
				if ([currentwordobject getType] == SUBHEADERTAG && [currentwordobject getLastword] == true) {
					cursorx = 10.0f;
					cursory += wordsize.height+3.0f;
				}
				
				//cursorx += 10.0f;
			}	
			
			
		} // for loop
		
	} // wordsavailable	
	
	NSLog (@"Page %i prepared. Word on Page: %i", wantedpage, [_pagearray count]);
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code.
	
	//NSLog (@"Draw rect Wordrect x: %f, y: %f, width: %f, height: %f", rect.origin.x, rect.origin.y, rect.size.width, rect.size.height);
	
	
	if (_ttcurrentword < [_pagearray count]) {
		
		SingleWord *currentwordobject = [_pagearray objectAtIndex:_ttcurrentword];
		
		NSString *wordstring = [currentwordobject getWordtext];
		CGRect wordrect = [currentwordobject getWordrect];		
		
		if ([currentwordobject getColor] == Blue) {
			[_lcarsstring setAlphastring:wordstring];
			[_lcarsstring setLocation:wordrect.origin];
			[_lcarsstring PaintString];
		} 
		
		if ([currentwordobject getColor] == Orange) {
			[_lcarsstringorange setAlphastring:wordstring];
			[_lcarsstringorange setLocation:wordrect.origin];
			[_lcarsstringorange PaintString];
		}
		
		if ([currentwordobject getColor] == Yellow) {
			[_lcarsstringyellow setAlphastring:wordstring];
			[_lcarsstringyellow setLocation:wordrect.origin];
			[_lcarsstringyellow PaintString];
		}
		
		
		_ttcurrentword++;
	}
}

- (bool) isPointWithinRect:(CGPoint)point:(CGRect)rect
{
	if (point.x >= rect.origin.x && point.x <= (rect.origin.x + rect.size.width)) {		
		if (point.y >= rect.origin.y && point.y <= (rect.origin.y + rect.size.height)) {
			return true;
		}		
	}
	
	return false;
}

- (void)checkWikiLinkTouched:(CGPoint)touchpoint
{
	if (_pagearray != nil) {
		
		int p = 0;
		
		for (p=0;p<[_pagearray count];p++) {
			
			SingleWord *currentwordobject = [_pagearray objectAtIndex:p];
			
			if ([currentwordobject getType] == WikiLink) {
			
				if ([self isPointWithinRect:touchpoint :[currentwordobject getWordrect]] == true) {
					
					// This word has been touched, so request a redirect.
					NSLog (@"Word touched: %@", [currentwordobject getWordtext]);
					
					[self playSinglesoundChOne:@"clockbutton"];
					
					[self sendRedirectrequest:[currentwordobject getTarget]];
					
					break;
				}
				
			}
		}
	}
}

// View Touch Handling
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
	//NSLog(@"Touches ended.");
	//NSLog(@"Touches began!!");
	
	NSArray *allTouches = [touches allObjects];
	UITouch *currentTouch;
	CGPoint currentPoint;
	
	//NSLog (@"Number of touches in array: %i", [allTouches count]);
	
	int t = 0;
	
	for (t=0;t<[allTouches count];++t)
	{
		currentTouch = [allTouches objectAtIndex:t];
		currentPoint = [currentTouch locationInView:self];
		
		[self checkWikiLinkTouched:currentPoint];
		
		//NSLog (@"Location of touch %i: %f, %f", t, currentPoint.x, currentPoint.y);
		
	}
}

- (void)dealloc {
    [super dealloc];
}


@end
