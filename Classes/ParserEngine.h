//
//  ParserEngine.h
//  LCARS Wikipedia
//
//  Created by Danny Draper on 29/01/2011.
//  Copyright 2011 CedeSoft Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ParserTag.h"
#import "TagType.h"

#define FILETAG			200
#define WIKILINKTAG			201

@protocol ParsingEngine

- (void)foundToken:(NSString *)tokencontents:(int)identifier;
- (void)parsingFinished;

@end


@interface ParserEngine : NSObject {

	
	id _delegateobject;
	NSMutableArray *_parsingtags;
	NSMutableArray *_replaceables;
	NSMutableArray *_preparsereplaceables;
	
	bool _taglistinitialised;
	bool _replaceablelistinitialised;
	bool _preparsereplaceablesinitialised;
	
	int _imaxtaglength;
}

- (void) parse:(NSString *)fullcontents;
- (void) testParse:(NSString *)contents;
- (void) setDelegate:(NSObject *)delegate;

- (bool) startsWith:(NSString *)contents:(NSString *)startswith;
- (TagType *) analyseTag:(int)parenttype : (NSString *)contents;
- (void) notifyToken:(NSString *) contents:(int)identifier;
- (void) notifyFinished;
- (NSString *) rightString:(NSString *)inputstring:(int)length;
- (NSString *) leftString:(NSString *)inputstring:(int)length;
- (NSString *) shortenfromRight:(NSString *)inputstring:(int)length;
- (NSString *) filterstring:(NSString *)inputstring:(NSString *)filter;
- (bool) containsString:(NSString *)contents :(NSString *)searchingfor;
- (void) addTokentag:(NSString *)startingtag:(NSString *)endingtag:(int)tokenidentifier;
- (void) addReplaceable:(NSString *)texttoreplace:(NSString *)replacewith;
- (void) addPreparseReplaceable:(NSString *)texttoreplace:(NSString *)replacewith;

- (void) initialiseTaglist;
- (void) initialiseReplaceablelist;
- (void) initialisePreparseReplaceablelist;

@end
