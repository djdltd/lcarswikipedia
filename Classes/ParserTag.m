//
//  ParserTag.m
//  LCARS Wikipedia
//
//  Created by Danny Draper on 29/01/2011.
//  Copyright 2011 CedeSoft Ltd. All rights reserved.
//

#import "ParserTag.h"


@implementation ParserTag

- (ParserTag *) initWithTag:(NSString *)startingtag:(NSString *)endingtag:(int)tokenidentifier:(bool)symmetricaltag
{
	self = [super init];
	
	if (self) {
		_startingtag = [[[NSString alloc] initWithString:startingtag] retain];
		_endingtag = [[[NSString alloc] initWithString:endingtag] retain];
		_tokenidentifier = tokenidentifier;
		_symmetricaltag = symmetricaltag;
		
		return self;
	} else {
		return nil;
	}
}

- (NSString *)getStarttag
{
	return _startingtag;
}

- (NSString *)getEndtag
{
	return _endingtag;
}

- (int) getIdentifier
{
	return _tokenidentifier;
}

- (bool) isSymmetric
{
	return _symmetricaltag;
}


@end
