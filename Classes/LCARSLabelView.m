//
//  LCARSLabelView.m
//  LCARS Wikipedia
//
//  Created by Danny Draper on 26/02/2011.
//  Copyright 2011 CedeSoft Ltd. All rights reserved.
//

#import "LCARSLabelView.h"


@implementation LCARSLabelView


- (id)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code.
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)coder
{
	if ((self = [super initWithCoder:coder])) {
		
		_captionstring = [[[NSMutableString alloc] init] retain];
		
		_lcarsstring = [[[SmallAlpha alloc] init] retain];
		
		[_lcarsstring Initialise:5.0f :3.0f :14.0f :8.0f :18.0f :19.0f :33.0f : -1.0f : 8.0f: true : false: @"UCTypeAlpha.png"];
		[_lcarsstring setLocation:CGPointMake (5.0f, 5.0f)];
		[_lcarsstring setVisible:true];
		
		NSLog (@"The LCARS label view has been initialised.");
	}
	return self;
}

- (NSString *) shortenfromRight:(NSString *)inputstring:(int)length
{
	if ([inputstring length] >= length) {	
		return [inputstring substringWithRange:NSMakeRange(0, [inputstring length]-length)];
	} else {
		return @"";
	}
}

- (void)backspaceCaption
{
	[_captionstring setString:[self shortenfromRight:_captionstring :1]];
	[_lcarsstring setAlphastring:_captionstring];
	[self setNeedsDisplay];
}

- (void)appendCaption:(NSString *)textvalue
{
	[_captionstring appendString:textvalue];
	[_lcarsstring setAlphastring:_captionstring];
	[self setNeedsDisplay];
}

- (void)setCaption:(NSString *)caption
{
	[_captionstring setString:caption];
	[_lcarsstring setAlphastring:_captionstring];
	[self setNeedsDisplay];
}

- (NSString *)getCaption
{
	return _captionstring;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code.
	
	//[_lcarsstring setAlphastring:@"This is a word"];
	[_lcarsstring PaintString];
}


- (void)dealloc {
    [super dealloc];
}


@end
