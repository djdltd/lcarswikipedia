//
//  TagType.h
//  LCARS Wikipedia
//
//  Created by Danny Draper on 12/02/2011.
//  Copyright 2011 CedeSoft Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface TagType : NSObject {

	int _tagtype;
	
	NSMutableString *_caption;
	NSMutableString *_target;
	
	bool _captioninitialised;
	bool _targetinitialised;
}

- (TagType *) initWithTag:(NSString *)caption :(NSString *)target :(int)type;

- (int) getType;
- (NSString *) getCaption;
- (NSString *) getTarget;
- (void) setType:(int)type;
- (void) setCaption:(NSString *)caption;
- (void) setTarget:(NSString *)target;

@end
