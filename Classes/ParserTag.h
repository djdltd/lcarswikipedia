//
//  ParserTag.h
//  LCARS Wikipedia
//
//  Created by Danny Draper on 29/01/2011.
//  Copyright 2011 CedeSoft Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface ParserTag : NSObject {
	
	NSString *_startingtag;
	NSString *_endingtag;
	int _tokenidentifier;
	bool _symmetricaltag;
	
}

- (ParserTag *) initWithTag:(NSString *)startingtag:(NSString *)endingtag:(int)tokenidentifier:(bool)symmetricaltag;

- (NSString *)getStarttag;
- (NSString *)getEndtag;
- (int) getIdentifier;
- (bool) isSymmetric;


@end
